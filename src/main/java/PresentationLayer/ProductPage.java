package PresentationLayer;

import DataAccess.ProductDAO;
import Model.Product;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 * The ProductPage class represents a dialog for displaying product information.
 * It extends JDialog and retrieves product data from the database.
 */
public class ProductPage extends JDialog{
    private JList productList;
    private JList quantityList;
    private JPanel contentPane;
    private JList priceList;
    private static final ProductDAO productDAO=new ProductDAO();

    /**
     * Constructs a ProductPage dialog.
     * @param parent The parent dialog
     */
    public ProductPage(JDialog parent) {
        super(parent);
        setTitle("Products");
        setContentPane(contentPane);
        setMinimumSize(new Dimension(500, 500));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        ArrayList<Product> products=(ArrayList<Product>) productDAO.findAll();
        DefaultListModel<String> listModel = new DefaultListModel<>();
        products.stream().map(Product::getName).forEach(listModel::addElement);
        productList.setModel(listModel);
        listModel = new DefaultListModel<>();
        products.stream().map(Product::getPrice).map(Objects::toString).forEach(listModel::addElement);
        priceList.setModel(listModel);
        listModel = new DefaultListModel<>();
        products.stream().map(Product::getQuantity).map(Objects::toString).forEach(listModel::addElement);
        quantityList.setModel(listModel);
        setVisible(true);
    }
}
