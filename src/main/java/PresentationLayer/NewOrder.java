package PresentationLayer;

import BussinessLogic.ProductBLL;
import DataAccess.BillDAO;
import DataAccess.OrderDAO;
import DataAccess.ProductDAO;
import Model.Bill;
import Model.Client;
import Model.Orders;
import Model.Product;

import javax.swing.*;
import java.awt.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/**
 * The NewOrder class represents a dialog for creating a new order.
 * It extends JDialog and allows the user to select products and place orders.
 */
public class NewOrder extends JDialog {
    private JPanel contentPane;
    private JList list1;
    private JList list2;
    private JPanel buttonPanel;
    private Client client;
    private static final ProductDAO productDAO = new ProductDAO();
    private static final OrderDAO orderDAO = new OrderDAO();
    private static final BillDAO billDAO = new BillDAO();

    /**
     * Constructs a NewOrder dialog.
     * @param parent The parent dialog
     * @param client The Client object representing the logged-in user
     */
    public NewOrder(JDialog parent, Client client) {
        super(parent);
        this.client = client;
        setTitle("Products");
        setContentPane(contentPane);
        setMinimumSize(new Dimension(500, 500));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        ArrayList<Product> products = (ArrayList<Product>) productDAO.findAll();
        DefaultListModel<String> listModel = new DefaultListModel<>();
        products.stream().map(Product::getName).forEach(listModel::addElement);
        list1.setFixedCellHeight(25);
        list2.setFixedCellHeight(25);
        list1.setModel(listModel);
        listModel = new DefaultListModel<>();
        products.stream().map(Product::getQuantity).map(Objects::toString).forEach(listModel::addElement);
        list2.setModel(listModel);
        buttonPanel.setLayout(new GridLayout(products.size(), 1));
        products.forEach(this::createButtonForProduct);
        setVisible(true);
    }

    private void createButtonForProduct(Product product) {
        JButton button = new JButton("Order Product");
        button.addActionListener(e -> {
            Integer inputQuantity;
            JTextField textField = new JTextField(5);
            Object[] message = {
                    "Enter quantity:", textField
            };
            int option = JOptionPane.showConfirmDialog(null, message, "Input", JOptionPane.OK_CANCEL_OPTION);
            if (option == JOptionPane.OK_OPTION) {
                try {
                    inputQuantity = Integer.valueOf(textField.getText());
                    ProductBLL.validateQuantity(product, inputQuantity);
                    product.setQuantity(product.getQuantity() - inputQuantity);
                    productDAO.updatebyId(product.getId(), product);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                    return;
                }
                JOptionPane.showMessageDialog(null, "Product Order Successfully created");
                Orders order = new Orders(product.getId(), client.getId(), Date.valueOf(LocalDate.now()));
                orderDAO.insert(order);
                Bill bill=new Bill(order.getId(),Date.valueOf(LocalDate.now()),product.getPrice()*inputQuantity);
                bill.writeBill();
                dispose();
                new Menu(null,client);
            }
        });
        buttonPanel.add(button);
    }
}
