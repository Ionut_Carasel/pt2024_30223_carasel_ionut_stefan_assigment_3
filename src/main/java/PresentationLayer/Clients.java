package PresentationLayer;

import DataAccess.ClientDAO;
import Model.Client;
import Model.Product;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Objects;

public class Clients extends JDialog{
    private JPanel contentPane;
    private JList idList;
    private JList firstNameList;
    private JList lastNameList;
    private JList emailList;
    private static final ClientDAO clientDAO = new ClientDAO();
    public Clients(JDialog parent) {
        super(parent);
        setTitle("Client Panel");
        setContentPane(contentPane);
        setMinimumSize(new Dimension(650, 500));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        ArrayList<Client> clients=(ArrayList<Client>) clientDAO.findAll();
        DefaultListModel<String> listModel = new DefaultListModel<>();
        clients.stream().map(Client::getId).map(Objects::toString).forEach(listModel::addElement);
        idList.setModel(listModel);
        listModel = new DefaultListModel<>();
        clients.stream().map(Client::getFirstName).forEach(listModel::addElement);
        firstNameList.setModel(listModel);
        listModel = new DefaultListModel<>();
        clients.stream().map(Client::getLastName).forEach(listModel::addElement);
        lastNameList.setModel(listModel);
        listModel = new DefaultListModel<>();
        clients.stream().map(Client::getMail).forEach(listModel::addElement);
        emailList.setModel(listModel);
        setVisible(true);
    }
}
