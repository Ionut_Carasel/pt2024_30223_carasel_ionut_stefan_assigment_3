package PresentationLayer;

import BussinessLogic.ClientBLL;
import BussinessLogic.Validators.MailValidator;
import DataAccess.ClientDAO;
import Model.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The LoginPage class represents a dialog for user login.
 * It extends JDialog and provides functionality for logging in with a registered email address.
 */
public class LoginPage extends JDialog{
    private JPanel content;
    private JTextField textField;
    private JButton loginButton;
    private JButton signUpButton;
    private JLabel loginMess;

    private ClientDAO clientDAO;

    /**
     * Constructs a LoginPage dialog.
     * @param parent The parent dialog
     */
    public LoginPage(JDialog parent) {
        super(parent);
        setTitle("Login");
        setContentPane(content);
        setMinimumSize(new Dimension(500, 500));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);


        loginButton.addActionListener(e -> {
            MailValidator validator = new MailValidator();
            String mail = textField.getText();
            try{
                validator.validateString(mail);
                clientDAO = new ClientDAO();
                Client client = clientDAO.findByMail(mail);
                if(client != null) {
                    dispose();
                    Menu menu = new Menu(parent,client);
                }
            }
            catch(Exception ex){
                loginMess.setText("Could not log in");
            }
        });
        setVisible(true);
    }
}
