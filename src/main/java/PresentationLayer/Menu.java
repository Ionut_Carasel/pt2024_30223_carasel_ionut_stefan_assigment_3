package PresentationLayer;

import Model.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The Menu class represents a main menu dialog for the user after logging in.
 * It extends JDialog and provides options to view orders, make a new order, and view products.
 */
public class Menu extends JDialog{
    private JPanel content;
    private JButton viewOrdersButton;
    private JButton makeANewOrderButton;
    private JButton productsButton;
    private JButton clientsButton;
    private Client client;

    /**
     * Constructs a Menu dialog.
     * @param parent The parent dialog
     * @param client The Client object representing the logged-in user
     */
    public Menu(JDialog parent,Client client){
        super(parent);
        this.client = client;
        setTitle("Sign in");
        setContentPane(content);
        setMinimumSize(new Dimension(500, 500));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        viewOrdersButton.addActionListener( e->{
            dispose();
            new OrderPanel(null);
        });
        productsButton.addActionListener(e -> {
            dispose();
            new ProductPage(null);
        });
        makeANewOrderButton.addActionListener(e -> {
            dispose();
            new NewOrder(null,client);
        });
        clientsButton.addActionListener(e -> {
            dispose();
            new Clients(null);
        });
        setVisible(true);

    }
}
