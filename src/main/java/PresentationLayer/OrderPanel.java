package PresentationLayer;

import DataAccess.OrderDAO;
import DataAccess.ProductDAO;
import Model.Orders;
import Model.Product;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 * The OrderPanel class represents a dialog for displaying orders.
 * It extends JDialog and retrieves order information from the database.
 */
public class OrderPanel extends JDialog {

    private JPanel contentPane;
    private JList list1;
    private JList list2;
    private static final OrderDAO orderDAO = new OrderDAO();
    private static final ProductDAO productDAO = new ProductDAO();

    /**
     * Constructs an OrderPanel dialog.
     * @param parent The parent dialog
     */
    public OrderPanel(JDialog parent) {
        super(parent);
        setTitle("Orders");
        setContentPane(contentPane);
        setMinimumSize(new Dimension(500, 500));
        setModal(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        ArrayList<Orders> orders=(ArrayList<Orders>) orderDAO.findAll();
        DefaultListModel<String> listModel = new DefaultListModel<>();
        orders.stream().map(Orders::getProduct_id).map(productDAO::findById).map(Product::getName).forEach(listModel::addElement);
        list1.setModel(listModel);
        listModel=new DefaultListModel<>();
        orders.stream().map(Orders::getOrderDate).map(Objects::toString).forEach(listModel::addElement);
        list2.setModel(listModel);
        setVisible(true);
    }
}
