package BussinessLogic.Validators;

/**
 * The Validator interface defines methods for validating objects of type T.
 *
 * @param <T> the type of object to validate
 */
public interface Validator<T> {
    /**
     * Validates an object of type T.
     *
     * @param t the object to validate
     */
    public void validate(T t);

    /**
     * Validates a string.
     *
     * @param s the string to validate
     */
    public void validateString(String s);
}
