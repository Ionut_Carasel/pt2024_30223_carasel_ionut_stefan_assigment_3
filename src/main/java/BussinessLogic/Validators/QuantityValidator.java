package BussinessLogic.Validators;

import Model.Product;

import java.util.regex.Pattern;

/**
 * The QuantityValidator class implements the Validator interface to validate the quantity of a product.
 * It checks whether the quantity is a positive integer.
 */

public class QuantityValidator implements Validator<Product> {
    private static final String QUANTITY_REGEX = "^[1-9][0-9]*$";

    /**
     * Validates the quantity of a product.
     *
     * @param t the product to validate
     * @throws IllegalArgumentException if the quantity is invalid
     */
    @Override
    public void validate(Product t) {
        Pattern pattern = Pattern.compile(QUANTITY_REGEX);
        if(!pattern.matcher(toString(t.getQuantity())).matches()){
            throw new IllegalArgumentException("Invalid Quantity");
        }
    }

    /**
     * Validates a string representing a quantity.
     *
     * @param s the string to validate
     * @throws IllegalArgumentException if the string is not a valid quantity
     */
    @Override
    public void validateString(String s) {
        Pattern pattern = Pattern.compile(QUANTITY_REGEX);
        if(!pattern.matcher(s).matches()){
            throw new IllegalArgumentException("Invalid Quantity");
        }
    }

    /**
     * Converts an integer quantity to a string.
     *
     * @param quantity the quantity to convert
     * @return the string representation of the quantity
     */
    private String toString(int quantity) {
        return Integer.toString(quantity);
    }
}