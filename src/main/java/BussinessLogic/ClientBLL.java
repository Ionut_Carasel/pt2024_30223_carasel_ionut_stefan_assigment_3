package BussinessLogic;

import java.util.*;

import BussinessLogic.Validators.*;
import DataAccess.ClientDAO;
import Model.Client;

/**
 * The ClientBLL class represents the business logic layer for managing Client entities.
 * It includes methods for finding clients by ID and managing validators.
 */

public class ClientBLL {
    private final List<Validator<Client>> validators;
    private final ClientDAO clientDAO;

    public ClientBLL() {
        validators = new ArrayList<>();
        validators.add(new MailValidator());
        clientDAO = new ClientDAO();
    }

    /**
     * Finds a client by their ID.
     *
     * @param id the ID of the client to find
     * @return the client with the specified ID
     * @throws NoSuchElementException if no client with the given ID is found
     */
    public Client findClientById(int id) {
        Client client = clientDAO.findById(id);
        if(client == null){
            throw new NoSuchElementException("There is no client with the id " + id);
        }
        return client;
    }

    /**
     * Gets the list of validators for clients.
     *
     * @return the list of validators
     */
    public List<Validator<Client>> getValidators() {
        return validators;
    }

    /**
     * Gets the ClientDAO instance used by the ClientBLL.
     *
     * @return the ClientDAO instance
     */
    public ClientDAO getClientDAO() {
        return clientDAO;
    }
}