package BussinessLogic;

import BussinessLogic.Validators.QuantityValidator;
import BussinessLogic.Validators.Validator;
import DataAccess.ProductDAO;
import Model.Product;

import java.util.NoSuchElementException;

/**
 * The ProductBLL class represents the business logic layer for managing Product entities.
 * It includes methods for finding products by ID or name, and validating product quantity.
 */

public class ProductBLL {
    private static final Validator<Product> validator=new QuantityValidator();
    private static final ProductDAO productDAO=new ProductDAO();

    /**
     * Finds a product by its ID.
     *
     * @param id the ID of the product to find
     * @return the product with the specified ID
     * @throws NoSuchElementException if no product with the given ID is found
     */
    public Product findProduct(int id) {
        Product product = productDAO.findById(id);
        if(product == null){
            throw new NoSuchElementException("There is no client with the id " + id);
        }
        return product;
    }

    /**
     * Finds a product by its name.
     *
     * @param productName the name of the product to find
     * @return the product with the specified name
     * @throws NoSuchElementException if no product with the given name is found
     */
    public Product findProduct(String productName) {
        Product product = productDAO.findProductByName(productName);
        if(product == null){
            throw new NoSuchElementException("There is no client with the name " + productName);
        }
        return product;
    }

    /**
     * Validates the quantity of a product.
     *
     * @param product  the product to validate
     * @param quantity the quantity to validate
     * @throws IllegalArgumentException if the quantity is not valid
     */
    public static void validateQuantity(Product product, int quantity) throws IllegalArgumentException {
        if (quantity <= 0) {
            throw new IllegalArgumentException("Quantity must be greater than 0");
        }else if(quantity > product.getQuantity()){
            throw new IllegalArgumentException("Quantity must be less than or equal to " + product.getQuantity());
        }
    }
}
