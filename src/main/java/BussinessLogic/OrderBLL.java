package BussinessLogic;

import DataAccess.OrderDAO;
import Model.Orders;
import java.util.*;

/**
 * The OrderBLL class represents the business logic layer for managing Order entities.
 * It includes methods for finding orders by ID.
 */

public class OrderBLL {
    private OrderDAO OrderDAO = new OrderDAO();

    public OrderBLL(){
        OrderDAO = new OrderDAO();
    }

    /**
     * Finds an order by its ID.
     *
     * @param id the ID of the order to find
     * @return the order with the specified ID
     * @throws NoSuchElementException if no order with the given ID is found
     */
    public Orders findOrders(int id){
        Orders Orders = OrderDAO.findById(id);
        if(Orders == null){
            throw new NoSuchElementException("Orders not found");
        }
        return Orders;
    }
}
