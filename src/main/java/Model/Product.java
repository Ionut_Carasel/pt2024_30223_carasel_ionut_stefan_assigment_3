package Model;

/**
 * The Product class represents a product with details such as an ID, name, quantity, and price.
 */

public class Product {
    private int id;
    private String name;
    private int quantity;
    private double price;

    public Product(int id, String name, int quantity, double price) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public Product() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public int availableQuantityForOrder(int desiredQuantity) {
        return Math.min(desiredQuantity, this.quantity);
    }

    public void sellProduct(int desiredQuantity) {
        if (desiredQuantity < this.quantity) {
            this.quantity -= desiredQuantity;
        }
        this.quantity = 0;
    }
}
