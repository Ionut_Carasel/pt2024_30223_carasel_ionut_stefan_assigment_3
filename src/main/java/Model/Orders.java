package Model;

import java.sql.Date;

/**
 * The Orders class represents an order with details about the product, client, order date, and delivery status.
 */

public class Orders {
    private int id;
    private int product_id;
    private int client_id;
    private Date orderDate;
    private boolean delivered;

    public Orders(int product, int client, Date date) {
        this.product_id = product;
        this.client_id = client;
        this.orderDate=date;
        this.delivered = false;
    }

    public Orders() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }
}
