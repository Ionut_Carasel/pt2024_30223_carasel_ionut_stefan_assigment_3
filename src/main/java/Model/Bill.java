package Model;

import DataAccess.ConnectionFactory;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;

/**
 * The Bill class represents a bill with details such as an ID, date, and price.
 * This class is implemented as a record, providing a compact syntax for immutable data objects.
 */
public record Bill(int id, Date date, double Price) {

    /**
     * Writes the bill to the database.
     * The bill details are inserted into the Bill table.
     */
    public void writeBill(){
        String sql="insert into Bill values(?,?,?)";
        Connection con= ConnectionFactory.getConnection();
        try{
            PreparedStatement ps= con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setDate(2, new Date(date.getTime()));
            ps.setDouble(3, Price);
            ps.executeUpdate();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @return the price of the bill
     */
    public double Price() {
        return Price;
    }

    public Date date() {
        return date;
    }

    public int id() {
        return id;
    }
}
