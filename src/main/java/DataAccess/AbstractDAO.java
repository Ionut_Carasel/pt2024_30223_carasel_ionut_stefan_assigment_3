package DataAccess;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The AbstractDAO class provides generic methods for performing CRUD operations on a database.
 * It uses reflection to handle various types of entities.
 *
 * @param <T> the type of the entity
 */

public class AbstractDAO<T> {
    private final Class<T> type;

    /**
     * Constructs a new AbstractDAO and determines the entity type using reflection.
     */
    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Creates a SQL select statement to find an entity by a specific field.
     *
     * @param field the field to use in the WHERE clause
     * @return the SQL select statement
     */
    protected String createSelectStatement(String field) {
        return "SELECT " +
                " * " +
                " FROM " +
                type.getSimpleName().toLowerCase() +
                " WHERE " + field.toLowerCase() + " =?";
    }

    /**
     * Creates a SQL insert statement for the given entity.
     *
     * @param t the entity to insert
     * @return the SQL insert statement
     * @throws IllegalAccessException if access to the field is denied
     */
    protected String createInsertStatement(T t) throws IllegalAccessException {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" (");
        for (int i = 1; i < type.getDeclaredFields().length; i++) {
            Field f = type.getDeclaredFields()[i];
            sb.append(f.getName().toLowerCase());
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(") VALUES (");
        for (int i = 1; i < type.getDeclaredFields().length; i++) {
            Field f = type.getDeclaredFields()[i];
            f.setAccessible(true);
            if (f.getType() == String.class || f.getType() == Date.class || f.getType() == LocalDateTime.class) {
                sb.append("'");
                sb.append(f.get(t));
                sb.append("'");
            } else {
                sb.append(f.get(t));
            }
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(")");
        return sb.toString();
    }

    /**
     * Creates a SQL update statement for the given field.
     *
     * @param field the field to use in the WHERE clause
     * @return the SQL update statement
     */
    protected String createUpdateStatement(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" SET ");
        for (int i = 1; i < type.getDeclaredFields().length; i++) {
            Field f = type.getDeclaredFields()[i];
            sb.append(f.getName().toLowerCase());
            sb.append(" = ");
            sb.append("?,");
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(" WHERE ");
        sb.append(type.getDeclaredFields()[0].getName().toLowerCase());
        sb.append(" = ");
        sb.append("?");
        return sb.toString();
    }

    /**
     * Creates a SQL delete statement for the given field.
     *
     * @param field the field to use in the WHERE clause
     * @return the SQL delete statement
     */
    protected String createDeleteStatement(String field) {
        return "DELETE FROM " +
                type.getSimpleName().toLowerCase() +
                " WHERE " +
                field.toLowerCase() +
                " = ?";
    }

    /**
     * Finds an entity by its ID.
     *
     * @param id the ID of the entity
     * @return the entity, or null if not found
     */
    public T findById(int id) {
        Connection connection = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet resultSet;
        String query = createSelectStatement(type.getDeclaredFields()[0].getName());
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            ConnectionFactory.closeConnection();
            e.printStackTrace();
        } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
            return null;
        }
        return null;
    }

    /**
     * Finds all entities.
     *
     * @return a list of all entities
     */
    public List<T> findAll() {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet resultSet;
        try {
            statement = con.prepareStatement("SELECT * FROM " + type.getSimpleName().toLowerCase());
            resultSet = statement.executeQuery();
        } catch (SQLException e) {
            ConnectionFactory.closeConnection();
            throw new RuntimeException(e);
        }

        return createObjects(resultSet);
    }

    /**
     * Creates a list of entities from the given ResultSet.
     *
     * @param resultSet the ResultSet to process
     * @return a list of entities
     */
    protected List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<>();
        Constructor<?>[] ctors = type.getDeclaredConstructors();
        Constructor<?> ctor = null;
        for (Constructor<?> constructor : ctors) {
            ctor = constructor;
            if (ctor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                ctor.setAccessible(true);
                T instance = (T) ctor.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(fieldName);
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException | IllegalAccessException | SecurityException | InvocationTargetException |
                 IllegalArgumentException | SQLException | IntrospectionException e) {
            System.err.println("Could not create Objects");
            ConnectionFactory.closeConnection();
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Inserts a new entity into the database.
     *
     * @param t the entity to insert
     * @return the generated key of the inserted entity
     */
    public int insert(T t) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement statement;
        ResultSet resultSet;
        int key = -1;
        try {
            statement = con.prepareStatement(createInsertStatement(t), Statement.RETURN_GENERATED_KEYS);
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                key = resultSet.getInt(1);
            }
        } catch (Exception e) {
            ConnectionFactory.closeConnection();
            e.printStackTrace();
        }
        return key;
    }

    /**
     * Updates an entity in the database by its ID.
     *
     * @param id        the ID of the entity to update
     * @param newObject the new entity data
     */
    public void updatebyId(int id, T newObject) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement statement;
        try {
            statement = con.prepareStatement(createUpdateStatement(type.getSimpleName().toLowerCase()));
            for (int i = 1; i < type.getDeclaredFields().length; i++) {
                Field f = type.getDeclaredFields()[i];
                f.setAccessible(true);
                statement.setObject(i, f.get(newObject));
            }
            statement.setInt(type.getDeclaredFields().length, id);
            statement.executeUpdate();
        } catch (Exception e) {
            System.err.println("Couldn't update object");
            ConnectionFactory.closeConnection();
            e.printStackTrace();
        }
    }

    /**
     * Deletes an entity from the database by its ID.
     *
     * @param id the ID of the entity to delete
     */
    public void deletebyId(int id) {
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement statement;
        try {
            Field f = type.getDeclaredFields()[0];
            statement = con.prepareStatement(createDeleteStatement(f.getName().toLowerCase()));
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (Exception e) {
            System.err.println("Couldn't delete object");
            ConnectionFactory.closeConnection();
            e.printStackTrace();
        }
    }
}