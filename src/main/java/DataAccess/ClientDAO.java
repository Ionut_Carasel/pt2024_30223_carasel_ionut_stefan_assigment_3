package DataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import Model.Client;

/**
 * The ClientDAO class extends AbstractDAO to provide specific data access operations for Client entities.
 */

public class ClientDAO extends AbstractDAO<Client> {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());

    private final String SELECT_MAIL = createSelectStatement("mail");

    /**
     * Finds a Client by their email address.
     *
     * @param mail the email address of the Client to find
     * @return the Client with the specified email address, or null if not found
     */
    public Client findByMail(String mail) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = SELECT_MAIL;

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1,mail);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        }catch (SQLException e) {
            LOGGER.log(Level.WARNING,"Client DAO: findByMail " + e.getMessage());
        }

        return null;
    }

}