package DataAccess;

import Model.Orders;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The OrderDAO class extends AbstractDAO to provide specific data access operations for Orders entities.
 */

public class OrderDAO extends AbstractDAO<Orders>{
    protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
}
