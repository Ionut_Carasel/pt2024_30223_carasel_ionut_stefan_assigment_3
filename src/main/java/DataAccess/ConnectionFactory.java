package DataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ConnectionFactory class provides methods to manage the database connection.
 * It uses a singleton pattern to ensure only one connection is created.
 */

public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DB_URL = "jdbc:mysql://localhost:3306/tp_project_schema";
    private static final String USER = "root";
    private static final String PASSWORD = "St3l@Arto1s";
    public static Connection connection = null;

    /**
     * Returns the database connection. If the connection does not already exist,
     * it creates a new one.
     *
     * @return the database connection
     */
    public static Connection getConnection() {
        if(connection==null) {
            try {
                connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    /**
     * Closes the database connection if it is open.
     */
    public static void closeConnection() {
        try {
            connection.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
