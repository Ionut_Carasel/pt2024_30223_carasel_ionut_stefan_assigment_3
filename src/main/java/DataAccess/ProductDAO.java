package DataAccess;

import Model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ProductDAO class extends AbstractDAO to provide specific data access operations for Product entities.
 */

public class ProductDAO extends AbstractDAO<Product>{
    private final String SELECT_BY_NAME = createSelectStatement("name");

    /**
     * Finds a Product by its name.
     *
     * @param name the name of the Product to find
     * @return the Product with the specified name, or null if not found
     */
    public Product findProductByName(String name){
        Product product = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = SELECT_BY_NAME;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, name);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        }
        catch (SQLException e){
            Logger.getLogger(ProductDAO.class.getName()).log(Level.WARNING,"Product ", "DAO: findByName"+e);
        }
        return product;
    }
}