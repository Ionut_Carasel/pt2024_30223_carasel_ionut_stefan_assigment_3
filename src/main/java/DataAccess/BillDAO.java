package DataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import Model.Bill;

/**
 * The BillDAO class extends AbstractDAO to provide specific data access operations for Bill entities.
 */
public class BillDAO extends AbstractDAO<Bill> {
}
